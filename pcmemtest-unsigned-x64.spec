# Prevent stripping
%global __spec_install_post /usr/lib/rpm/brp-compress
# Turn off debuginfo package
%global debug_package %{nil}

Name:           pcmemtest-unsigned-x64
Version:        1.4
Release:        1%{?dist}
Summary:        Stand-alone memory tester for x86 and x86-64 architecture computers

License:        GPLv2 and GPLv3
URL:            https://github.com/martinwhitaker/pcmemtest
Source0:        https://github.com/martinwhitaker/pcmemtest/archive/v%{version}/pcmemtest-%{version}.tar.gz
Source1:        20_pcmemtest
BuildRequires:  gcc make
ExclusiveArch:  x86_64

%description
PCMemTest is a stand-alone memory tester for x86 and x86-64 architecture
computers. It provides a more thorough memory check than that provided by
BIOS memory tests.


%package efi
Summary: EFI version of pcmemtest
Requires: %{name}%{?_isa} = %{version}-%{release}


%description efi
PCMemTest is a stand-alone memory tester for x86 and x86-64 architecture
computers.

This package provides the EFI version of PCMemTest.


%package bios
Summary: BIOS version of pcmemtest
Requires: %{name}%{?_isa} = %{version}-%{release}


%description bios
PCMemTest is a stand-alone memory tester for x86 and x86-64 architecture
computers.

This package provides the BIOS version of PCMemTest.


%prep
%autosetup -n pcmemtest-%{version}


%build
cd build64
# Regular build flags are not wanted for this binary
make


%install
cd build64
mkdir -p %{buildroot}/{boot,%{efi_esp_dir}}
install -m 0644 memtest.efi %{buildroot}/%{efi_esp_dir}/pcmemtest.efi
install -m 0644 memtest.bin %{buildroot}/boot/pcmemtest.bin

mkdir -p %{buildroot}%{_sysconfdir}/grub.d
install -m755 %{SOURCE1} %{buildroot}%{_sysconfdir}/grub.d


%files
%license LICENSE
%doc README.md


%files efi
%{efi_esp_dir}/pcmemtest.efi


%files bios
%{_sysconfdir}/grub.d/20_pcmemtest
/boot/pcmemtest.bin


%changelog
* Sat Aug 28 2021 Gordon Messmer <gordon.messmer@gmail.com>
- Initial package
